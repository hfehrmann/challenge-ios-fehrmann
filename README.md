Aquí se entrega el desafío

Para poder correr esta aplicación es necesario tener instalado cocoapods. Esto se puede realizar a través de los gems con el siguiente comando: sudo gem install cocoapods.

Una vez instalado cocoapods, en el directorio del projecto se debe realizar el siguiente comando: pod install. Este comando genera un archivo .xcworkspace el que debe ser utilizado en el futuro en vez de .xcodeproj.

Para esta aplicación se consideró versiones de iOS +10.0, por lo que solo corre en iphone 5 en adelante.
