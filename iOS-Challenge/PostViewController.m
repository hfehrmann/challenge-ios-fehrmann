//
//  PostViewController.m
//  iOS-Challenge
//
//  Created by Hans on 3/19/18.
//  Copyright © 2018 TechKiOSChallenge-Fehrmann. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PostViewController.h"
#import "iOS_Challenge-Swift.h"

@implementation PostViewController

-(void)post {
    NSMutableDictionary *postParams = [[NSMutableDictionary alloc] init];
    
    NSData *imageData =  UIImagePNGRepresentation(self.image);
    NSString *base64Image = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    [postParams setValue:base64Image forKey:@"image"];
    [postParams setValue:@"base64" forKey:@"type"];
    
    NSString *text = self.titleInput.text;
    if (text != nil && ([text length] != 00)) { [postParams setValue:text forKey:@"title"]; }
    
    NSString *album = self.albumInput.text;
    if (album != nil && [album length] != 0) { [postParams setValue:album forKey:@"album"]; }
    
    NSString *description = self.descriptionInput.text;
    if (description != nil && ([description length] != 0)) { [postParams setValue:description forKey:@"description"]; }
    
    IMGURRequest *request = [[IMGURRequest alloc] init];
    [request postWithUrlString:@"https://api.imgur.com/3/image" additionalParameters:@{} postParams:postParams
            completion:^(NSData * _Nullable data, NSURLResponse * _Nullable resp, NSError * _Nullable err) { }];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Post" style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(post)];
    self.imageView.image = self.image;
    
    self.titleInput.delegate = self;
    self.albumInput.delegate = self;
    self.descriptionInput.delegate = self;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        CGFloat width = tableView.bounds.size.width / 2;
        return width * self.image.size.height / self.image.size.width;
    } else {
        return [super tableView:tableView heightForRowAtIndexPath:indexPath];
    }
}

-(void)textViewDidEndEditing:(UITextView *)textView {
    [textView resignFirstResponder];
}

@end

