//
//  WatchCollectionViewCell.h
//  iOS-Challenge
//
//  Created by Hans on 3/19/18.
//  Copyright © 2018 TechKiOSChallenge-Fehrmann. All rights reserved.
//

#ifndef WatchCollectionViewCell_h
#define WatchCollectionViewCell_h


#endif /* WatchCollectionViewCell_h */

#import <UIKit/UIKit.h>

@interface WatchTableViewCell: UITableViewCell

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loader;
@property (weak, nonatomic) IBOutlet UIImageView *imgurImage;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@end
