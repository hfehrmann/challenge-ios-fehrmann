//
//  WatchImageViewController.h
//  iOS-Challenge
//
//  Created by Hans on 3/18/18.
//  Copyright © 2018 TechKiOSChallenge-Fehrmann. All rights reserved.
//

#ifndef WatchImageViewController_h
#define WatchImageViewController_h


#endif /* WatchImageViewController_h */

#import <UIKit/UIKit.h>

@interface WatchImageViewController: UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic) NSArray *images;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
