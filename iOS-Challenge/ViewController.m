//
//  ViewController.m
//  iOS-Challenge
//
//  Created by Hans on 3/18/18.
//  Copyright © 2018 TechKiOSChallenge-Fehrmann. All rights reserved.
//

#import "ViewController.h"
#import <Realm/Realm.h>

#import "Tags.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if (![defaults boolForKey:@"First_run"]) {
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:@"https://api.imgur.com/3/tags"]];
        [request setValue:@"Client-ID cc9dbf432a92691" forHTTPHeaderField:@"Authorization"];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:
            ^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                NSError *erro = nil;
                NSMutableArray *results = [[NSMutableArray alloc] init];
                if (data != nil) {
                    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&erro];
                    NSDictionary *data = [json objectForKey:@"data"];
                    NSArray *tags = [data objectForKey:@"tags"];
                    for (NSDictionary *tag in tags) {
                        Tag *realmTag = [[Tag alloc] init];
                        realmTag.key_name = [tag objectForKey:@"name"];
                        realmTag.name = [tag objectForKey:@"display_name"];
                        [results addObject:realmTag];
                    }
                }
                RLMRealm *realm = [RLMRealm defaultRealm];
                [realm transactionWithBlock: ^{
                    for (Tag *realmTag in results) {
                        [realm addObject:realmTag];
                    }
                    [defaults setBool:YES forKey:@"First_run"];
                    NSLog(@"setted first run");
                }];
                dispatch_async(dispatch_get_main_queue(), ^{
                    @autoreleasepool {
                        [self performSegueWithIdentifier:@"toTagSegue" sender:self];
                    }
                }) ;
            }];
        [task resume];
    } else {
        [self performSegueWithIdentifier:@"toTagSegue" sender:self];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
