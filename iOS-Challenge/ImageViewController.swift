//
//  ImageViewController.swift
//  iOS-Challenge
//
//  Created by Hans on 3/19/18.
//  Copyright © 2018 TechKiOSChallenge-Fehrmann. All rights reserved.
//

import Foundation
import UIKit

class ImageViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var imageToLoad: SimpleIMGURImage!
    
    var comments: [(author: String, text: String)] = []
    
    override func viewDidLoad() {
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
        
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.tableHeaderView = UIView()
        tableView.tableFooterView = UIView()
        
        let commentsURL = "https://api.imgur.com/3/gallery/\(imageToLoad.id)/comments/best"
        
        IMGURRequest().get(urlString: commentsURL, additionalParameters: [:]) { (data, resp, err) in
            guard let data = data,
                  let jsonRaw = try? JSONSerialization.jsonObject(with: data, options: []),
                  let json = jsonRaw as? [String: Any],
                  let commentsRaw = json["data"] as? NSArray else { return }
            
            var results: [(author: String, text: String)] = []
            for case let comment as [String: Any] in commentsRaw {
                guard let author = comment["author"] as? String, let comment = comment["comment"] as? String else {
                    continue
                }
                results.append(author: author, text: comment)
            }
            self.comments = results
            DispatchQueue.main.async {
                self.tableView.reloadSections(IndexSet([1]), with: .automatic)
            }
        }
    }
    
    fileprivate var inHierarchy: Bool = true
    override func viewWillDisappear(_ animated: Bool) {
        if let viewControllers = self.navigationController?.viewControllers {
            if let _ = viewControllers.index(of: self) {
                inHierarchy = true
            } else {
                inHierarchy = false
            }
        }
    }
    
}

extension ImageViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (section == 0) ? 1 : comments.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = indexPath.section
        if section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ImageInfoCell") as! ImageInfoTableViewCell
            cell.setImage(image: imageToLoad.image, widthToHeightRatio: imageToLoad.height / imageToLoad.width)
            if imageToLoad.imageState.isLoading() {
                cell.loader.startAnimating()
            } else {
                cell.loader.stopAnimating()
            }
            imageToLoad.onFinishDownloading {
                if self.inHierarchy { tableView.reloadRows(at: [indexPath], with: .automatic) }
            }
            cell.upvotes.text = "\(imageToLoad.upvotes)"
            cell.downvotes.text = "\(imageToLoad.downvotes)"
            cell.views.text = "\(imageToLoad.views)"
            cell.descriptionLabel.text = imageToLoad.imageDescription
            return cell
        } else {
            let comment = comments[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell") as! CommentTableViewCell
            cell.authorLabel.text = comment.author
            cell.commentLabel.text = comment.text
            return cell
        }
        
    }
}

extension ImageViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }
}
