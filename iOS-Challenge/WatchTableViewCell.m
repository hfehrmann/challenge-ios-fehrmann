//
//  WatchCollectionViewCell.m
//  iOS-Challenge
//
//  Created by Hans on 3/19/18.
//  Copyright © 2018 TechKiOSChallenge-Fehrmann. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WatchTableViewCell.h"

@implementation WatchTableViewCell

-(void)awakeFromNib {
    [super awakeFromNib];
    self.loader.hidesWhenStopped = YES;
}

@end
