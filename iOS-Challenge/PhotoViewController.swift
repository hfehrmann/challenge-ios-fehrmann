//
//  PhotoViewController.swift
//  iOS-Challenge
//
//  Created by Hans on 3/19/18.
//  Copyright © 2018 TechKiOSChallenge-Fehrmann. All rights reserved.
//

import Foundation
import UIKit
import Photos

class PhotoViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var allPhotos: PHFetchResult<PHAsset>!
    var images: [UIImage?] = []
    
    static fileprivate let maxWidth: CGFloat = 75
    static fileprivate let minWidth: CGFloat = 65
    
    let imagePicker = UIImagePickerController()
    dynamic func takePicture() {
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .camera
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .camera, target: self, action: #selector(takePicture))
        imagePicker.delegate = self
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        let options = PHFetchOptions()
        options.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: true)]
        
        var count = 0
        
        allPhotos = PHAsset.fetchAssets(with: options)
        let manager = PHImageManager.default()
        allPhotos.enumerateObjects(using: { (asset, i, pointer) in
            manager.requestImage(for: asset, targetSize: CGSize(width:90,height:90), contentMode: .aspectFit, options: nil, resultHandler: { (image, dict) in
                self.images.append(image)
                count += 1
                if count == self.allPhotos.count - 1 {
                    self.collectionView.reloadData()
                }
            })
        })
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        collectionView.reloadData()
    }
    
    fileprivate func sendImage(image: UIImage) {
        let photoVC = storyboard?.instantiateViewController(withIdentifier: "PostViewController") as! PostViewController
        photoVC.image = image
        self.navigationController?.pushViewController(photoVC, animated: true)
    }
}

extension PhotoViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Photo", for: indexPath) as! PhotoCollectionViewCell
        cell.image.image = images[indexPath.row]
        return cell
    }
}

extension PhotoViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let asset = allPhotos[indexPath.row]
        let manager = PHImageManager.default()
        manager.requestImage(for: asset, targetSize: PHImageManagerMaximumSize, contentMode: .aspectFit, options: nil) { (image, dict) in
            if let image = image {
                self.sendImage(image: image)
            }
        }
    }
    
    public func collectionView(_ collectionView: UICollectionView,
                               layout collectionViewLayout: UICollectionViewLayout,
                               sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width
        let smallNum = floor(width / PhotoViewController.maxWidth)
        let bigNum = floor(width / PhotoViewController.minWidth)
        let num = (bigNum > smallNum) ? bigNum : smallNum
        let photoWidth = (width - 2*num) / num
        return CGSize(width: photoWidth, height: photoWidth)
    }
    
    public func collectionView(_ collectionView: UICollectionView,
                               layout collectionViewLayout: UICollectionViewLayout,
                               minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    public func collectionView(_ collectionView: UICollectionView,
                               layout collectionViewLayout: UICollectionViewLayout,
                               minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    public func collectionView(_ collectionView: UICollectionView,
                               layout collectionViewLayout: UICollectionViewLayout,
                               insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 2, left: 0, bottom: 0, right: 0)
    }
}

extension PhotoViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.sendImage(image: pickedImage)
        }
    }
    
}

