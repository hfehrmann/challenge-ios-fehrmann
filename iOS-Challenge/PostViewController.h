//
//  PostViewController.h
//  iOS-Challenge
//
//  Created by Hans on 3/19/18.
//  Copyright © 2018 TechKiOSChallenge-Fehrmann. All rights reserved.
//

#ifndef PostViewController_h
#define PostViewController_h


#endif /* PostViewController_h */

#import <UIKit/UIKit.h>

@interface PostViewController : UITableViewController <UITextFieldDelegate>

@property (nonatomic) UIImage *image;

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UITextField *titleInput;
@property (weak, nonatomic) IBOutlet UITextField *albumInput;
@property (weak, nonatomic) IBOutlet UITextField *descriptionInput;

@end
