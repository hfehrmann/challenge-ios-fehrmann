//
//  Tags.m
//  iOS-Challenge
//
//  Created by Hans on 3/18/18.
//  Copyright © 2018 TechKiOSChallenge-Fehrmann. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

@interface Tag: RLMObject
@property NSString *key_name;
@property NSString *name;
@end

@implementation Tag
+ (NSArray *)requiredProperties {
    return @[@"key_name", @"name"];
}
@end
