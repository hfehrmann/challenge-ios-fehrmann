//
//  IMGURRequest.swift
//  iOS-Challenge
//
//  Created by Hans on 3/18/18.
//  Copyright © 2018 TechKiOSChallenge-Fehrmann. All rights reserved.
//

import Foundation

class IMGURRequest: NSObject {
    
    @objc func get(urlString uString: String, additionalParameters: NSDictionary,
                    completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
        let request = NSMutableURLRequest(url: URL(string: uString as String)!)
        request.setValue("Client-ID cc9dbf432a92691", forHTTPHeaderField: "Authorization")
        for (k,v) in additionalParameters {
            request.setValue(v as? String, forHTTPHeaderField: k as! String)
        }
        let session = URLSession(configuration: URLSessionConfiguration.default)
        let task = session.dataTask(with: request as URLRequest, completionHandler: completion)
        task.resume()
    }
    
    @objc func post(urlString uString: String, additionalParameters: NSDictionary, postParams: NSDictionary,
                    completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
        let request = NSMutableURLRequest(url: URL(string: uString as String)!)
        request.httpMethod = "POST"
        request.setValue("Client-ID cc9dbf432a92691", forHTTPHeaderField: "Authorization")
        for (k,v) in additionalParameters {
            request.setValue(v as? String, forHTTPHeaderField: k as! String)
        }
        //let post = postParams.map { "\($0)=\($1)" }.joined(separator: "&")
        request.httpBody = try? JSONSerialization.data(withJSONObject: postParams, options: [])
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        let session = URLSession(configuration: URLSessionConfiguration.default)
        let task = session.dataTask(with: request as URLRequest, completionHandler: completion)
        task.resume()
    }
}
