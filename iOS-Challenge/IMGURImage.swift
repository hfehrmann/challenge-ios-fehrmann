//
//  IMGURImage.swift
//  iOS-Challenge
//
//  Created by Hans on 3/18/18.
//  Copyright © 2018 TechKiOSChallenge-Fehrmann. All rights reserved.
//

import Foundation


@objc enum IMGURImageState: Int {
    case New, Fetching, Downloaded, Failed
    
    func isLoading() -> Bool {
        switch self{
        case .New, .Fetching:
            return true
        default:
            return false
        }
    }
}


class SimpleIMGURImage: NSObject {
    
    let id: String
    let title: String
    let imageDescription: String
    let date: Date
    let upvotes: Int
    let downvotes: Int
    let views: Int
    
    let height: CGFloat
    let width: CGFloat
    
    private let link: URL
    private var loaded_image: UIImage!
    private var completionsOnDownload: [(Void) -> Void] = []
    private var _imageState: IMGURImageState = .New
    var imageState: IMGURImageState { get{ return _imageState }}
    var image: UIImage {
        get {
            switch _imageState {
            case .Downloaded:
                return loaded_image
            case .New:
                _imageState = .Fetching
                DispatchQueue.global(qos: .background).async {
                    guard let data = try? Data(contentsOf: self.link), let image = UIImage(data: data) else {
                        self._imageState = .Failed
                        return
                    }
                    self.loaded_image = image
                    self._imageState = .Downloaded
                    DispatchQueue.main.async {
                        let completions = self.completionsOnDownload
                        self.completionsOnDownload = []
                        _ = completions.map { $0() }
                    }
                }
                fallthrough
            default:
                return UIImage(named: "black")!
            }
        }
    }
    
    init?(data dictionary: [String: Any]) {
        guard let images = dictionary["images"] as? NSArray else { return nil }
        guard images.count == 1 else { return nil }
        guard let image = images[0] as? [String: Any] else { return nil }
        guard let urlString = image["link"] as? String else { return nil }
        guard let urlData = URL(string: urlString) else { return nil }
        link = urlData
        
        guard let idData = dictionary["id"] as? String else { return nil }
        id = idData
        
        guard let titleData = dictionary["title"] as? String else { return nil }
        title = titleData
        
        imageDescription = (image["description"] as? String) ?? ""
        
        guard let upvotesData = dictionary["ups"] as? Int else { return nil }
        upvotes = upvotesData
        
        guard let downvotesData = dictionary["downs"] as? Int else { return nil }
        downvotes = downvotesData
        
        guard let dateTimes = dictionary["datetime"] as? Int else { return nil }
        date = Date(timeIntervalSince1970: TimeInterval(dateTimes))
        
        guard let viewsData = image["views"] as? Int else { return nil }
        views = viewsData
        
        guard let widthData = image["width"] as? Int else { return nil }
        width = CGFloat(widthData)
        
        guard let heightData = image["height"] as? Int else { return nil }
        height = CGFloat(heightData)
    }
    
    func onFinishDownloading(completion: @escaping (Void) -> Void) {
        switch imageState {
        case .New, .Fetching:
            self.completionsOnDownload.append(completion)
        default:
            break
        }
    }
}
