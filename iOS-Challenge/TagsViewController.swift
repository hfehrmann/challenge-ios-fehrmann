//
//  TagsViewController.swift
//  iOS-Challenge
//
//  Created by Hans on 3/18/18.
//  Copyright © 2018 TechKiOSChallenge-Fehrmann. All rights reserved.
//

import Foundation
import UIKit

import Realm

class TagsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    
    var allTags: [(key: String, name: String)] = []
    lazy var filterTags: [(key: String, name: String)] = {return self.allTags}()
    let searchController = UISearchController(searchResultsController: nil)
    
    dynamic func addTapped() {
        self.performSegue(withIdentifier: "toAddPhoto", sender: nil)
        
    }
    
    override func viewDidLoad() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTapped))
        
        loader.stopAnimating()
        let allRealmTags =  (Tag.allObjects() as! RLMResults<Tag>)
        for case let realTag as Tag in allRealmTags {
            allTags.append((key:realTag.key_name, name:realTag.name))
        }
        allTags.sort(by: {(s1, s2) in s1.name.compare(s2.name) == ComparisonResult.orderedAscending})
        
        title = "Imgur Tags"
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
        view.addSubview(searchController.searchBar)
        
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Buscar tag"
        definesPresentationContext = true
        
        tableView.tableHeaderView = searchController.searchBar
        tableView.tableFooterView = UIView()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        let refresh = UIRefreshControl()
        refresh.addTarget(self, action: #selector(self.refresh), for: .valueChanged)
        tableView.refreshControl = refresh
    }
    
    dynamic func refresh() {
        IMGURRequest().get(urlString: "https://api.imgur.com/3/tags", additionalParameters: [:])
        { (data, response, error) in
            if let data = data {
                guard let jsonSerial = try? JSONSerialization.jsonObject(with: data, options: []) else { return }
                guard let json = jsonSerial as? [String:Any] else { return }
                guard let jsonData = json["data"] as? [String:Any] else { return }
                guard let tags = jsonData["tags"] as? [[String:Any]] else { return }
                let realm = RLMRealm.default()
                realm.beginWriteTransaction()
                for tag in tags {
                    let key = tag["name"] as! String
                    if (!self.allTags.contains(where: {t in t.key == key})) {
                        let name = tag["display_name"] as! String
                        let realmTag = Tag()
                        realmTag.key_name = key
                        realmTag.name = name
                        realm.add(realmTag)
                        if let index = self.allTags.index(where: {s in s.name.compare(name) == ComparisonResult.orderedDescending}) {
                            self.allTags.insert((key: key, name: name), at: index)
                        } else {
                            self.allTags.append((key: key, name: name))
                        }
                    }
                }
                try! realm.commitWriteTransaction()
            }
            DispatchQueue.main.async {
                self.refilterResults(for: self.searchController)
                self.tableView.reloadData()
                self.tableView.refreshControl?.endRefreshing()
            }

        }
    }
    
    fileprivate func refilterResults(for searchController: UISearchController) {
        let search = searchController.searchBar.text!
        if search == "" {
            filterTags = allTags
        } else {
            filterTags = allTags.filter { $0.name.lowercased().contains(search.lowercased()) }
        }
    }
}

extension TagsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        loader.startAnimating()
        let tag = filterTags[indexPath.row]
        let urlString = String(format: "https://api.imgur.com/3/gallery/t/%@/viral/day/1", tag.key)
        IMGURRequest().get(urlString: urlString, additionalParameters: [:]) { (data, resp, err) in
            guard let data = data else { return }
            guard let jsonData = try? JSONSerialization.jsonObject(with: data, options: []) else { return }
            guard let json = jsonData as? [String:Any]  else { return }
            guard let tagData = json["data"] as? [String: Any] else { return }
            guard let items = tagData["items"] as? NSArray else { return }
            var result: [SimpleIMGURImage?] = []
            for case let post as [String: Any] in items {
                result.append(SimpleIMGURImage(data: post))
            }
            let imageResult = result.flatMap { $0 }
            
            let stb = self.storyboard
            let watchVC = stb?.instantiateViewController(withIdentifier: "WatchImageViewController") as! WatchImageViewController
            watchVC.title = tag.name.capitalized
            watchVC.images = imageResult
            DispatchQueue.main.async {
                self.loader.stopAnimating()
                self.navigationController?.pushViewController(watchVC, animated: true)
            }
        }
    }
}

extension TagsViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterTags.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TagCell")!
        let name = filterTags[indexPath.row].name
        cell.textLabel?.text = name.capitalized
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        let row = indexPath.row
        let tag = filterTags[row]
        let key_name = tag.key
        allTags = allTags.filter { $0.key != key_name }
        filterTags.remove(at: row)
        let realm = RLMRealm.default()
        if let tag = Tag.objects(where: "key_name = %@", key_name).firstObject() {
            realm.beginWriteTransaction()
            realm.delete(tag)
            try! realm.commitWriteTransaction()
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
        
    }
    
}

extension TagsViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        self.refilterResults(for: searchController)
        tableView.reloadData()
    }
    
}
