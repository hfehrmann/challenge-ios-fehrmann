//
//  PhotoCollectionViewCell.swift
//  iOS-Challenge
//
//  Created by Hans on 3/19/18.
//  Copyright © 2018 TechKiOSChallenge-Fehrmann. All rights reserved.
//

import Foundation
import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
    
}
