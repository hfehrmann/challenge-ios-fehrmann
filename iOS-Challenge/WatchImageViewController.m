//
//  WatchImageViewController.m
//  iOS-Challenge
//
//  Created by Hans on 3/18/18.
//  Copyright © 2018 TechKiOSChallenge-Fehrmann. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WatchTableViewCell.h"
#import "WatchImageViewController.h"
#import "iOS_Challenge-Swift.h"

@implementation WatchImageViewController {
    NSDateFormatter *formatter;
}

-(void)viewDidLoad {
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy"];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:nil action:nil];
}

#pragma UITableViewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.images count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    WatchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WatchCell"];
    SimpleIMGURImage *image = [self.images objectAtIndex:indexPath.row];
    cell.imgurImage.image = image.image;
    if (image.imageState == IMGURImageStateFailed || image.imageState == IMGURImageStateDownloaded) {
        [cell.loader stopAnimating];
    } else {
        [cell.loader startAnimating];
        [image onFinishDownloadingWithCompletion:^{
            NSArray *viewControllers = self.navigationController.viewControllers;
            if (viewControllers != nil && [viewControllers indexOfObject:self] != NSNotFound){
                [tableView reloadRowsAtIndexPaths: @[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            }
        }];
    }
    cell.titleLabel.text = image.title;
    cell.descriptionLabel.text = image.imageDescription;
    cell.dateLabel.text = [formatter stringFromDate:image.date];
    return cell;
}

#pragma UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 150;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    ImageViewController *imageController = [self.storyboard instantiateViewControllerWithIdentifier:@"ImageViewController"];
    SimpleIMGURImage *image = [self.images objectAtIndex:indexPath.row];
    imageController.imageToLoad = image;
    imageController.title = image.title;
    [self.navigationController pushViewController:imageController animated:YES];
}

@end

