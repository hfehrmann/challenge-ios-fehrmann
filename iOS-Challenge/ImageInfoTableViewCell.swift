//
//  ImageInfoTableViewCell.swift
//  iOS-Challenge
//
//  Created by Hans on 3/19/18.
//  Copyright © 2018 TechKiOSChallenge-Fehrmann. All rights reserved.
//

import Foundation
import UIKit

class ImageInfoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var mainImage: UIImageView!
    @IBOutlet weak var upvotes: UILabel!
    @IBOutlet weak var downvotes: UILabel!
    @IBOutlet weak var views: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var HeightImageConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        loader.hidesWhenStopped = true
    }
    
    func setImage(image: UIImage, widthToHeightRatio: CGFloat) {
        let width = mainImage.frame.width
        let newHeight = width * widthToHeightRatio
        HeightImageConstraint.constant = newHeight
        mainImage.image = image
    }
}
