//
//  CommentTableViewCell.swift
//  iOS-Challenge
//
//  Created by Hans on 3/19/18.
//  Copyright © 2018 TechKiOSChallenge-Fehrmann. All rights reserved.
//

import Foundation
import UIKit

class CommentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    
    override func awakeFromNib() {
        viewContainer.layer.borderWidth = 0.5
        viewContainer.layer.borderColor = UIColor.black.cgColor
    }
}
